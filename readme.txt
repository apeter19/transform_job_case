
Implement changes to code base for data transform job
----------------------------------------

This project contains the code base for a data transform job used in two projects: ProjectA and ProjectB

The base class 'TransfomJobBase' (located in ./job_base/base_job.py) is used as a super class for the data transform 
job of the two projects.

The run script for ProjectA and ProjectB can be found in /projectA/projectA_job.py and ./projectB/projectB_job.py,
respectively. These can be executed from within the folders by running 'python projectX_job.py'
 
The two projects have each formulated new requirements. To fulfill these, it will be necessary to change the code base.

Requirements:

- ProjectA: Currently, only one file is read from the input directory. Change this so all files in the input directory
are read, transformed, and written when the job is executed.
- ProjectB: Currently, the job writes csv-files. This should be changed to parquet-files, i.e. ProjectB should output
parquet files instead of csv-files.

Importantly, changes to functionality requested by one project should not interfere with the other
project (for example ProjectA should still write csv-files, and ProjectB should still only read one file).
