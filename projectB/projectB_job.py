import sys
sys.path.append('../')
import pandas as pd
from job_base import TransformJobBase, Reader, Writer

class ProjectBJob(TransformJobBase):
    def __init__(self, reader: Reader, writer: Writer):
        super().__init__("ProjectBJob", reader, writer)

    def _transform(self, data: pd.DataFrame) -> pd.DataFrame:
        data['Diff'] = data['Max'] - data['Min']
        return data


if __name__ == "__main__":
    job = ProjectBJob.create()
    job.execute()
