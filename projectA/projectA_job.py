import sys
sys.path.append('../')
import pandas as pd
from datetime import datetime
from job_base import TransformJobBase, Reader, Writer


class ProjectAJob(TransformJobBase):
    def __init__(self, reader: Reader, writer: Writer):
        super().__init__("ProjectAJob", reader, writer)

    def _transform(self, data: pd.DataFrame) -> pd.DataFrame:
        data['Timestamp'] = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        return data


if __name__ == "__main__":
    job = ProjectAJob.create()
    job.execute()
