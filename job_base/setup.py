﻿from setuptools import setup, find_packages

requires = [
    "pandas",
]

setup(
    name="jobbase",
    version="1.0.0",
    description="A",
    author="Ørsted",
    packages=find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operating System :: OS Independent",
    ],
    install_requires=requires,
    tests_requires=requires + ['pytest']
)
