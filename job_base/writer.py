import os
import pandas as pd
from datetime import datetime


class Writer:
    def __init__(self):
        cwd = os.getcwd()
        self._output_path = os.path.join(cwd, "output")
        self._processed_path = os.path.join(cwd, "processed")
        self._base_file_name = "out.csv"

    def _log(self, message: str):
        print(message)

    def _get_output_path(self, base_name: str):
        return os.path.join(self._output_path, self._prepend_datetime(base_name))

    def _prepend_datetime(self, s: str):
        now = datetime.now().strftime('%Y-%m-%d_%H-%M-%S')
        return "{}_{}".format(now, s)

    def write_data(self, data: pd.DataFrame):
        output_path = self._get_output_path(self._base_file_name)
        data.to_csv(output_path, index=False)
        self._log("Wrote {} rows to {}".format(data.shape[0], output_path))

