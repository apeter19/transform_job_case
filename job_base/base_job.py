import pandas as pd
from abc import ABC, abstractmethod

from .reader import Reader
from .writer import Writer


class TransformJobBase(ABC):
    """
    An abstract base class that data transformation jobs should inherit from.

    Methods
    -------
    execute()
        Executes the job.
    """
    def __init__(self, name: str, reader: Reader, writer: Writer):
        self.name = name
        self._reader = reader
        self._writer = writer

    @classmethod
    def create(cls):
        return cls(Reader(), Writer())

    def _log(self, message: str):
        """
        Logs a message by writing it to the console.
        """
        print(message)

    def execute(self):
        """
        Executes the job by reading raw data, transforming it, and writing it to disc.
        """
        self._log("Started {}".format(self.name))
        raw_data = self._reader.read_data()
        transformed_data = self._transform(raw_data)
        self._writer.write_data(transformed_data)
        self._log("Finished {}".format(self.name))

    @abstractmethod
    def _transform(self, data: pd.DataFrame) -> pd.DataFrame:
        """
        Abstract method that should be overwritten by project specific code.
        The implementation should transform raw data into the desired form.
        """
        pass
    

