import os
import pandas as pd


class Reader:
    def __init__(self):
        cwd = os.getcwd()
        self._input_path = os.path.join(cwd, "input")

    def _log(self, message: str):
        print(message)

    def _get_source_file_path(self) -> str:
        files = os.listdir(self._input_path)
        csv_files = [file for file in files if file.endswith(".csv")]
        path = os.path.join(self._input_path, csv_files[0])
        return path

    def read_data(self) -> pd.DataFrame:
        path = self._get_source_file_path()
        self._log("Reading data from {}".format(path))
        data = pd.read_csv(path)  # FIXME
        self._log("Read {} rows".format(data.shape[0]))
        return data

